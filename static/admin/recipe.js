(function () {

    function loadScript(src, onload) {
        var script = document.createElement("script");
        if (typeof onload === "function") {
            script.onload = onload;
        }
        script.src = src;
        document.head.appendChild(script);
    }

    // load js and css for summernote
    loadScript("https://code.jquery.com/jquery-3.2.1.slim.min.js", function () {
        loadScript("https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js", function () {
            $(document).ready(function() {
                $('#description').summernote();
            });
        });
    });

    var stylesheet = document.createElement("link");
    stylesheet.setAttribute("href", "https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css");
    stylesheet.setAttribute("rel", "stylesheet");
    document.head.appendChild(stylesheet);

}());
