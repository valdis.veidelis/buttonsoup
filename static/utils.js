function classNames() {
    var classesArray = Array.from(arguments).reduce(function (memo, curr) {
        if (typeof curr === "string") {
            memo.push(curr);
        } else if (Array.isArray(curr)) {
            curr.forEach(function (entry) {
                memo = memo.concat(classNames(entry));
            });
        } else if (typeof curr === "object") {
            Object.keys(curr).forEach(function(key) {
                if (curr[key]) {
                    memo.push(key);
                }
            });
        }
        return memo;
    }, []);

    return classesArray.join(" ");
}

function createElement(type, attributes, children) {
    var element = document.createElement(type);

    Object.keys(attributes).forEach(function(key) {
        if (key === "className") {
            classNames(attributes[key]).forEach(function (className) {
                element.classList.add(className);
            });
        } else if (key.indexOf("on") === 0) {
            element.addEventListener(key.replace("on", ""), attributes[key], false);
        } else {
            element.setAttribute(key, attributes[key]);
        }
    });

    if (Array.isArray(children)) {
        children.forEach(function(child) {
            child && element.appendChild(child);
        });
    } else if (typeof children === "string") {
        element.textContent = children;
    } else if (children) {
        element.appendChild(children);
    }

    return element;
}

function xhr(method, payload, url, callback) {
    const xhr = new XMLHttpRequest();
    xhr.onreadystatechange = () => {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            callback(xhr.responseText);
        }
    };
    xhr.open(method, url, true);
    xhr.setRequestHeader("Content-type", "application/json");
    xhr.send(JSON.stringify(payload));
}

var xhrPost = xhr.bind(undefined, "POST");

var xhrGet = xhr.bind(undefined, "GET", undefined);

function renderMessages(container, messages, type) {
    container.innerHTML = "";
    messages.forEach(function(message) {
        container.appendChild(createElement("li", { className: type }, message));
    });
}

function capitalize(string) {
    return string.slice(0, 1).toUpperCase() + string.slice(1).toLowerCase();
}

function roundTo(n, decimals) {
    var base = Math.pow(10, decimals);
    return Math.round(n * base) / base;
}
