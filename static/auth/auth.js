var domElements = {
    loginContainer: document.querySelector(".loginContainer"),
    submitLogin: document.querySelector("[name=login_submit]"),
    submitRegister: document.querySelector("[name=register_submit]"),
};

domElements.submitRegister.addEventListener("click", function () {
    var email = domElements.loginContainer.querySelector("[name=register_email]");
    var password1 = domElements.loginContainer.querySelector("[name=register_password1]");
    var password2 = domElements.loginContainer.querySelector("[name=register_password2]");
    var name = domElements.loginContainer.querySelector("[name=register_name]");
    var messagesContainer = domElements.loginContainer.querySelector(".register .messages");
    var errors = [];

    if (password1.value !== password2.value) {
        errors.push("Passwords must match");
        password1.value = "";
        password2.value = "";
        password1.focus();
    }

    if (email.value && email.value.indexOf("@") === -1) {
        errors.push("Invalid e-mail");
    }

    if (!password1.value || password1.value.length < 8) {
        errors.push("Password must be at least 8 characters long");
    }

    if (!name.value) {
        errors.push("Name must be supplied");
    }

    if (!email.value) {
        errors.push("E-mail must be supplied");
    }

    if (errors.length) {
        renderMessages(messagesContainer, errors, "error");
    } else {
        var payload = JSON.stringify({
            name: name.value,
            email: email.value,
            password1: password1.value,
            password2: password2.value,
        });

        messagesContainer.innerHTML = "";

        xhrPost(payload, "/auth/register", function (response) {
            var responseJson = JSON.parse(response);
            renderMessages(messagesContainer, responseJson.messages, responseJson.type);
            [email, password1, password2, name].forEach(function(element) {
                element.value = "";
            });
        });
    }
});

domElements.submitLogin.addEventListener("click", function () {
    var email = domElements.loginContainer.querySelector("[name=login_email]");
    var password = domElements.loginContainer.querySelector("[name=login_password]");
    var messagesContainer = domElements.loginContainer.querySelector(".login .messages");
    var errors = [];

    if (email.value && email.value.indexOf("@") === -1) {
        errors.push("Invalid e-mail");
        email.focus();
    }

    if (errors.length) {
        renderMessages(messagesContainer, errors, "error");
    } else {
        var payload = JSON.stringify({
            email: email.value,
            password: password.value,
        });

        messagesContainer.innerHTML = "";

        xhrPost(payload, "/auth/login", function (response) {
            var responseJson = JSON.parse(response);
            renderMessages(messagesContainer, responseJson.messages, responseJson.type);

            if (responseJson.type === "success") {
                window.setTimeout(function () {
                    domElements.loginContainer.classList.remove("visible");
                    [email, password].forEach(function(element) {
                        element.value = "";
                    });
                    messagesContainer.innerHTML = "";

                    window.location = "/admin";
                }, 1000);
            }
        });
    }
});
