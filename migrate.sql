ALTER TABLE recipes ADD COLUMN IF NOT EXISTS thumbnail text;
ALTER TABLE ingredients ADD COLUMN IF NOT EXISTS grams_per_unit int;

CREATE TABLE IF NOT EXISTS labels(
    label_id serial NOT NULL,
    name varchar NOT NULL,
    CONSTRAINT label_id PRIMARY KEY (label_id),
    CONSTRAINT labels_unique_name UNIQUE (name)
);

ALTER TABLE labels OWNER TO buttonsoup;

CREATE TABLE IF NOT EXISTS recipes_labels(
	fk_recipe_id bigint NOT NULL,
	fk_label_id bigint NOT NULL,
	recipes_labels_id serial NOT NULL,
	CONSTRAINT recipes_labels_pk PRIMARY KEY (recipes_labels_id)
);

ALTER TABLE recipes_labels OWNER TO buttonsoup;

DO $$
BEGIN
    IF NOT EXISTS (
        SELECT NULL FROM information_schema.TABLE_CONSTRAINTS
        WHERE CONSTRAINT_SCHEMA = 'public' AND CONSTRAINT_NAME = 'fk_recipe_id' AND CONSTRAINT_TYPE = 'FOREIGN KEY'
    )
    THEN
        ALTER TABLE recipes_labels ADD CONSTRAINT fk_recipe_id FOREIGN KEY (fk_recipe_id) REFERENCES recipes (recipe_id);
    END IF;
END $$;

DO $$
BEGIN
    IF NOT EXISTS (
        SELECT NULL FROM information_schema.TABLE_CONSTRAINTS
        WHERE CONSTRAINT_SCHEMA = 'public' AND CONSTRAINT_NAME = 'fk_label_id' AND CONSTRAINT_TYPE = 'FOREIGN KEY'
    )
    THEN
        ALTER TABLE recipes_labels ADD CONSTRAINT fk_label_id FOREIGN KEY (fk_label_id) REFERENCES labels (label_id);
    END IF;
END $$;

ALTER TABLE recipes_ingredients ADD COLUMN IF NOT EXISTS amount TEXT;
ALTER TABLE recipes_ingredients ALTER COLUMN amount TYPE TEXT;
ALTER TABLE recipes_ingredients ADD COLUMN IF NOT EXISTS note TEXT;
ALTER TABLE recipes ALTER COLUMN grams_per_serving DROP NOT NULL;
ALTER TABLE recipes DROP COLUMN IF EXISTS thumbnail;
ALTER TABLE recipes ADD COLUMN IF NOT EXISTS thumbnail_url VARCHAR(100);
ALTER TABLE accounts ALTER COLUMN password_hash TYPE VARCHAR(60);
ALTER TABLE ingredients ALTER COLUMN protein DROP NOT NULL;
ALTER TABLE ingredients ALTER COLUMN fat DROP NOT NULL;
ALTER TABLE ingredients ALTER COLUMN carbohydrates DROP NOT NULL;
ALTER TABLE ingredients ADD COLUMN IF NOT EXISTS thumbnail_url VARCHAR(100);


