#CentOS

## app server setup
useradd buttonsoup
passwd buttonsoup
cd /home/buttonsoup
su - buttonsoup
git clone https://github.com/vikingface/buttonsoup.git
su - root
sudo yum -y update
sudo yum install -y python-devel python-setuptools python-pip
sudo yum install epel-release
sudo yum -y install yum-utils
sudo yum -y groupinstall development
sudo yum -y install python36
sudo yum -y install python36-pip
sudo yum -y install python36-devel
sudo pip install --upgrade pip

cd /home/buttonsoup
su - buttonsoup
python3.6 -m venv buttonsoupenv
source buttonsoupenv/bin/activate
cd /home/buttonsoup/buttonsoup
pip install -r requirements.txt

## web server setup (gunicorn & nginx)
sudo yum install nginx
sudo systemctl start nginx
cd /etc/systemd/system
vim buttonsoup.service
"""i
[Unit]
Description=Gunicorn instance to serve flask app
After=network.target

[Service]
User=buttonsoup
Group=buttonsoup
WorkingDirectory=/home/buttonsoup/buttonsoup
Environment="DATABASE_URL=postgresql://buttonsoup:*****@127.0.0.1:5432/buttonsoup"
Environment="PATH=/home/buttonsoup/buttonsoupenv"
RuntimeDirectoryMode=755
ExecStart=/home/buttonsoup/buttonsoupenv/bin/gunicorn --workers 3 --bind 127.0.0.1:5001 -m 007 app:app --error-logfile /home/buttonsoup/gunicorn.log --log-level debug

[Install]
WantedBy=multi-user.target
""":wq

sudo systemctl start buttonsoup
sudo systemctl stop buttonsoup
sudo systemctl restart buttonsoup
sudo systemctl status buttonsoup
sudo systemctl enable buttonsoup # start service on reboot
cd /etc/nginx/
vim conf.d buttonsoupcom.conf
"""
server {
    listen 80;
    listen [::]:80;
    server_name buttonsoup.com;

    access_log  /var/log/nginx/buttonsoup.access.log;
    error_log   /var/log/nginx/buttonsoup.error.log;

    location / {
        proxy_pass http://127.0.0.1:5001/;
        proxy_redirect   off;
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}
"""
sudo nginx -t
sudo service nginx restart
curl localhost:5001

## PostgreSQL setup
rpm -Uvh https://yum.postgresql.org/11/redhat/rhel-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm
yum install postgresql11-server postgresql11 -y
/usr/pgsql-11/bin/postgresql-11-setup initdb
systemctl enable postgresql-11.service
systemctl start postgresql-11.service
systemctl restart postgresql-11.service
systemctl status postgresql-11.service
vim /var/lib/pgsql/11/data/pg_hba.conf
"""

"""
psql -U postgres
CREATE DATABASE buttonsoup;
CREATE USER buttonsoup WITH PASSWORD 'buttonsoup';
ALTER DATABASE buttonsoup OWNER TO buttonsoup;
GRANT ALL PRIVILEGES ON DATABASE buttonsoup TO buttonsoup;
ALTER USER buttonsoup WITH PASSWORD '*****';
exit;
export DATABASE_URL=postgresql://buttonsoup:*****@127.0.0.1:5432/buttonsoup

psql -f buttonsoup.sql -U buttonsoup -d buttonsoup
psql -f migrate.sql -U buttonsoup -d buttonsoup
