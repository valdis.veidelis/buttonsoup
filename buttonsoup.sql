-- Database generated with pgModeler (buttonsoupQL Database Modeler).
-- pgModeler  version: 0.9.1
-- buttonsoupQL version: 10.0
-- Project Site: pgmodeler.io
-- Model Author: ---


-- Database creation must be done outside a multicommand file.
-- These commands were put in this file only as a convenience.
-- -- object: new_database | type: DATABASE --
-- -- DROP DATABASE IF EXISTS new_database;
-- CREATE DATABASE new_database;
-- -- ddl-end --
--

-- object: public.account_id_sequence | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS public.account_id_sequence CASCADE;
CREATE SEQUENCE public.account_id_sequence
	INCREMENT BY 1
	MINVALUE 0
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --
ALTER SEQUENCE public.account_id_sequence OWNER TO buttonsoup;
-- ddl-end --

-- object: public.ingredients | type: TABLE --
-- DROP TABLE IF EXISTS public.ingredients CASCADE;
CREATE TABLE public.ingredients(
	ingredient_id serial NOT NULL,
	name varchar(100) NOT NULL,
	vegan bool NOT NULL,
	vegetarian bool NOT NULL,
	one_in_five_a_day bool NOT NULL,
	probiotic bool NOT NULL,
	protein smallint NOT NULL,
	fat smallint NOT NULL,
	carbohydrates smallint NOT NULL,
	CONSTRAINT ingredient_id PRIMARY KEY (ingredient_id),
	CONSTRAINT ingredients_unique_name UNIQUE (name)

);
-- ddl-end --
ALTER TABLE public.ingredients OWNER TO buttonsoup;
-- ddl-end --

-- object: public.recipes | type: TABLE --
-- DROP TABLE IF EXISTS public.recipes CASCADE;
CREATE TABLE public.recipes(
	recipe_id serial NOT NULL,
	name varchar NOT NULL,
	grams_per_serving smallint NOT NULL,
	description text NOT NULL,
	CONSTRAINT recipe_id PRIMARY KEY (recipe_id),
	CONSTRAINT recipes_unique_name UNIQUE (name)

);
-- ddl-end --
ALTER TABLE public.recipes OWNER TO buttonsoup;
-- ddl-end --

-- object: public.recipes_ingredients | type: TABLE --
-- DROP TABLE IF EXISTS public.recipes_ingredients CASCADE;
CREATE TABLE public.recipes_ingredients(
	fk_recipe_id bigint NOT NULL,
	fk_ingredient_id bigint NOT NULL,
	recipes_ingredients_id serial NOT NULL,
	CONSTRAINT recipes_ingredients_pk PRIMARY KEY (recipes_ingredients_id)

);
-- ddl-end --
ALTER TABLE public.recipes_ingredients OWNER TO buttonsoup;
-- ddl-end --

-- object: public.accounts | type: TABLE --
-- DROP TABLE IF EXISTS public.accounts CASCADE;
CREATE TABLE public.accounts(
	account_id serial NOT NULL,
	name varchar(100) NOT NULL,
	email varchar(100) NOT NULL,
	password_hash varchar(20) NOT NULL,
	admin bool,
	CONSTRAINT account_id PRIMARY KEY (account_id),
	CONSTRAINT accounts_unique_email UNIQUE (email)

);
-- ddl-end --
ALTER TABLE public.accounts OWNER TO buttonsoup;
-- ddl-end --

-- object: fk_recipe_id | type: CONSTRAINT --
-- ALTER TABLE public.recipes_ingredients DROP CONSTRAINT IF EXISTS fk_recipe_id CASCADE;
ALTER TABLE public.recipes_ingredients ADD CONSTRAINT fk_recipe_id FOREIGN KEY (fk_recipe_id)
REFERENCES public.recipes (recipe_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: fk_ingredient_id | type: CONSTRAINT --
-- ALTER TABLE public.recipes_ingredients DROP CONSTRAINT IF EXISTS fk_ingredient_id CASCADE;
ALTER TABLE public.recipes_ingredients ADD CONSTRAINT fk_ingredient_id FOREIGN KEY (fk_ingredient_id)
REFERENCES public.ingredients (ingredient_id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --


