import json

from flask import Blueprint, session, redirect, request, jsonify, send_from_directory

from .models import DatabaseSession, Account

def init_auth_module(app):

    @app.route('/auth/admin', methods=('GET', 'POST'))
    def admin():
        if not session or not session['authenticated']:
            return redirect("/admin", code=302)
        elif request.method == 'POST':
            name = request.form['name']
            email = request.form['email']
            database_session = DatabaseSession()
            valid = True

            # TODO improve validation logic by making it shorter
            if not name:
                flash('Name must be supplied', 'error')
                valid = False
            if not email:
                flash('E-mail must be supplied', 'error')
                valid = False
            if name and len(name) > 100:
                flash('Name may not be longer than 100 characters', 'error')
                valid = False
            if email and len(email) > 100:
                flash('E-mail may not be longer than 100 characters', 'error')
                valid = False
            if database_session.query(Account.email).filter(Account.email == email).scalar() and session['email'] != email:
                flash('Such email is already registered', 'error')
                valid = False

            if valid:
                account = database_session.query(Account).filter(Account.email == session['email']).first()
                account.email = email
                account.name = name
                session['name'] = name
                session['email'] = email
                database_session.commit()

                flash('Account successfully updated')

            return redirect("/admin", code=302)

    @app.route('/auth/register', methods=['POST'])
    def register():
       data = json.loads(request.get_json())
       password1 = data.get('password1')
       password2 = data.get('password2')
       name = data.get('name')
       email = data.get('email')
       database_session = DatabaseSession()
       errors = []

       if not password1 or len(password1) == 0:
           errors.append("Password must be supplied")
       if password1 != password2:
           errors.append("Passwords must match")
       if not name:
           errors.append('Name must be supplied')
       if not email:
           errors.append('E-mail must be supplied')
       if password1 and len(password1) < 8:
           errors.append('Password must be at least 8 characters long')
       if password1 and len(password1) > 100:
           errors.append('Password may not be longer than 100 characters')
       if name and len(name) > 100:
           errors.append('Name may not be longer than 100 characters')
       if email and len(email) > 100:
           errors.append('E-mail may not be longer than 100 characters')
       if database_session.query(Account.email).filter(Account.email == email).scalar():
           errors.append('Such email is already registered')

       if len(errors) == 0:
           password_hash = app.bcrypt.generate_password_hash(password1).decode('utf-8')
           account = Account(name, email, password_hash)
           database_session.add(account)
           database_session.commit()
           database_session.close()

           return jsonify({ 'type': 'success', 'messages': ['User successfully registered, You may log in'] })
       else:
           return jsonify({ 'type': 'error', 'messages': errors })

    @app.route('/auth/login', methods=['GET', 'POST'])
    def login():
        if request.method == 'GET':
            return send_from_directory('./static/auth', 'auth.html')
        elif request.method == 'POST':
            data = json.loads(request.get_json())
            password = data.get('password')
            email = data.get('email')
            database_session = DatabaseSession()
            errors = []

            account_query = database_session.query(Account).filter(Account.email == email)
            account = account_query.first()

            if not account_query.scalar() or not app.bcrypt.check_password_hash(account.password_hash, password):
                errors.append('Incorrect email / password. Please try again')

            database_session.close()

            if len(errors) == 0:
                session['authenticated'] = True
                session['name'] = account.name
                session['email'] = account.email
                session['admin'] = bool(account.admin)

                return jsonify({ 'type': 'success', 'messages': ['Welcome, ' + account.name] })
            else:
                return jsonify({ 'type': 'error', 'messages': errors })
