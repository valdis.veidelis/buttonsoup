from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from .base import Base

class Label(Base):
    __tablename__ = 'labels'

    label_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(100), nullable=False)
    recipes = relationship("RecipeLabel", back_populates="label")

    def __str__(self):
        return self.name
