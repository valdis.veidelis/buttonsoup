from sqlalchemy import Table, Column, Integer, String, Boolean, Text, ForeignKey, LargeBinary
from sqlalchemy.orm import relationship

from .base import Base

class Recipe(Base):
    __tablename__ = 'recipes'

    recipe_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(100), nullable=False)
    description = Column(Text, nullable=False)
    grams_per_serving = Column(Integer)
    thumbnail_url = Column(String(100))

    ingredients = relationship("RecipeIngredient", back_populates="recipe", cascade="all, delete")
    labels = relationship("RecipeLabel", back_populates="recipe", cascade="all, delete")

    def __str__(self):
        return self.name

# Peel the potatoes, slice them in similar size chunks, put them in boiling water. Add some salt. Check ready-ness by poking with a knife or fork.
