from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, Text
from sqlalchemy.orm import relationship

from .base import Base

class RecipeIngredient(Base):
    __tablename__ = 'recipes_ingredients'

    fk_recipe_id = Column(Integer, ForeignKey('recipes.recipe_id'))
    fk_ingredient_id = Column(Integer, ForeignKey('ingredients.ingredient_id'))
    recipes_ingredients_id = Column(Integer, primary_key=True, autoincrement=True)
    amount = Column(Integer)
    note = Column(Text)

    ingredient = relationship("Ingredient", back_populates="recipes")
    recipe = relationship("Recipe", back_populates="ingredients")
