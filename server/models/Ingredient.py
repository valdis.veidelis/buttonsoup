from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.orm import relationship

from .base import Base

class Ingredient(Base):
    __tablename__ = 'ingredients'

    ingredient_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(100), nullable=False)
    vegan = Column(Boolean, nullable=False)
    vegetarian = Column(Boolean, nullable=False)
    one_in_five_a_day = Column(Boolean, nullable=False)
    probiotic = Column(Boolean, nullable=False)
    protein = Column(Integer)
    fat = Column(Integer)
    carbohydrates = Column(Integer)
    grams_per_unit = Column(Integer, nullable=True)
    thumbnail_url = Column(String(100))

    recipes = relationship("RecipeIngredient", back_populates="ingredient")

    def __str__(self):
        return self.name
