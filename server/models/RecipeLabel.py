from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship

from .base import Base

class RecipeLabel(Base):
    __tablename__ = 'recipes_labels'

    fk_recipe_id = Column(Integer, ForeignKey('recipes.recipe_id'))
    fk_label_id = Column(Integer, ForeignKey('labels.label_id'))
    recipes_labels_id = Column(Integer, primary_key=True, autoincrement=True)

    label = relationship("Label", back_populates="recipes")
    recipe = relationship("Recipe", back_populates="labels")
