from sqlalchemy import (Column, Integer, String, Boolean)

from .base import database


class Account(database.Model):
    __tablename__ = 'accounts'

    account_id = Column(Integer, unique=True, primary_key=True)
    name = Column(String, nullable=False)
    email = Column(String, nullable=False)
    admin = Column(Boolean, nullable=True)
    password_hash = Column(String, nullable=False)

    def __init__(self, name, email, password_hash, admin = False):
        self.name = name
        self.email = email
        self.admin = admin
        self.password_hash = password_hash
