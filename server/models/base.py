import os

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine import create_engine
from sqlalchemy.orm import sessionmaker, relationship

database = SQLAlchemy()

if os.environ.get('DATABASE_URL') is None:
    db_url = "postgresql://buttonsoup:buttonsoup123@127.0.0.1:5432/buttonsoup"
else:
    db_url = os.environ.get('DATABASE_URL')

engine = create_engine(db_url, convert_unicode=True)
DatabaseSession = sessionmaker(bind=engine)

Base = declarative_base()
