import os
import uuid

from werkzeug import secure_filename
from flask_admin.form.upload import ImageUploadField
from flask_admin.contrib.sqla import ModelView
from flask_admin import Admin
from flask import url_for

from .models import database, DatabaseSession, Account, RecipeIngredient, RecipeLabel, Recipe, Ingredient, Label

def generate_thumbnail_file_name(obj, file_data):
    return secure_filename('%s%s' % (uuid.uuid4().hex, os.path.splitext(file_data.filename)[1]))

class RecipeAdmin(ModelView):
    inline_models = (RecipeIngredient, RecipeLabel)
    form_extra_fields = {
        'thumbnail_url': ImageUploadField(
            'Thumbnail',
            base_path=os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'static', 'images', 'recipes', 'thumbnails'),
            namegen=generate_thumbnail_file_name,
            permission=438,
            url_relative_path=os.path.join('..', 'images', 'recipes', 'thumbnails', ''),
        )
    }
    def render(self, template, **kwargs):
        self.extra_js = [url_for("static", filename="admin/recipe.js")]
        return super(RecipeAdmin, self).render(template, **kwargs)

class IngredientAdmin(ModelView):
    form_excluded_columns = ('recipes',)
    form_columns = (
        'name', 'thumbnail_url', 'vegan', 'vegetarian', 'one_in_five_a_day',
        'probiotic', 'protein', 'fat', 'carbohydrates', 'grams_per_unit',
    )
    form_extra_fields = {
        'thumbnail_url': ImageUploadField(
            'Thumbnail',
            base_path=os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', 'static', 'images', 'ingredients', 'thumbnails'),
            namegen=generate_thumbnail_file_name,
            permission=438,
            url_relative_path=os.path.join('..', 'images', 'ingredients', 'thumbnails', ''),
        )
    }

class LabelView(ModelView):
    form_columns = ('name',)

def init_admin_module(app):
    admin = Admin(app, name='buttonsoup', template_mode='bootstrap2', index_view=None)
    admin.add_view(ModelView(Account, database.session))
    admin.add_view(RecipeAdmin(Recipe, database.session))
    admin.add_view(IngredientAdmin(Ingredient, database.session))
    admin.add_view(LabelView(Label, database.session))
