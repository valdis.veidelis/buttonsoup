#!/usr/bin/python3

import json
import os, sys

from flask import Flask, request, send_from_directory, jsonify
from flask_bcrypt import Bcrypt
from flask_marshmallow import Marshmallow, fields
from sqlalchemy.sql import text

from server.models import database, DatabaseSession, Ingredient, Recipe, RecipeIngredient
from server import init_auth_module, init_admin_module

app = Flask(__name__, static_url_path=None, static_folder='static')
app.bcrypt = Bcrypt(app)
marshmallow = Marshmallow(app)

init_auth_module(app)
init_admin_module(app)

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
if os.environ.get('DATABASE_URL') is None:
    app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://buttonsoup:buttonsoup123@127.0.0.1:5432/buttonsoup"
else:
    app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL')

app.secret_key = os.urandom(24)

# https://docs.sqlalchemy.org/en/13/_modules/examples/association/basic_association.html

# TODO
# validate summernote input with bleach
# load summernote css and js from /static
# create "premium" column for recipes table with type boolean to distinguish recipes for paying customers

database.init_app(app)

@app.route('/')
def index():
    return send_from_directory('.', 'index.html')

def rowToDict(row):
    return { c.name: getattr(row, c.name) for c in row.__table__.columns }

class IngredientSchema(marshmallow.ModelSchema):
    class Meta:
        model = Ingredient

class RecipeSchema(marshmallow.ModelSchema):
    class Meta:
        model = Recipe

ingredient_schema = IngredientSchema()
recipe_schema = RecipeSchema()

@app.route('/admin/recipes/<id>', methods=['GET'])
def admin_recipes_id(id):
    dump = recipe_schema.dump(DatabaseSession().query(Recipe).filter(Recipe.recipe_id == id).first())
    return jsonify({ 'description': dump.data['description'] })

@app.route('/admin/ingredients/all', methods=['GET'])
def admin_ingredients_all():
    return jsonify([rowToDict(row) for row in DatabaseSession().query(Ingredient).all()])

@app.route('/admin/all_recipes_and_ingredients', methods=['GET'])
def admin_all_recipes_and_ingredients():
    recipes_query_string = text("""
        SELECT R.RECIPE_ID, R.NAME, R.GRAMS_PER_SERVING, R.THUMBNAIL_URL, ARRAY_AGG(JSON_BUILD_OBJECT(
            'ingredient_id', I.INGREDIENT_ID,
            'amount', RI.AMOUNT,
            'note', RI.NOTE
        )) AS INGREDIENTS
        FROM RECIPES R
        LEFT JOIN RECIPES_INGREDIENTS RI ON RI.FK_RECIPE_ID = R.RECIPE_ID
        LEFT JOIN INGREDIENTS I ON RI.FK_INGREDIENT_ID = I.INGREDIENT_ID
        GROUP BY R.RECIPE_ID;
    """)

    recipes = [dict(row) for row in DatabaseSession().execute(recipes_query_string).fetchall()]
    ingredients = [rowToDict(row) for row in DatabaseSession().query(Ingredient).all()]

    return jsonify({ 'ingredients': ingredients, 'recipes': recipes })

@app.route('/admin/ingredients/<id>', methods=['GET'])
def admin_ingredients_id(id):
    return jsonify(ingredient_schema.dump(DatabaseSession().query(Ingredient).filter(Ingredient.ingredient_id == id).first()).data)

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5001))
    app.run(host='0.0.0.0', port=port)
