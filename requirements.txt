bcrypt==3.1.6
cffi==1.12.2
Click==7.0
Flask==1.0.2
Flask-Admin==1.5.3
Flask-Bcrypt==0.7.1
Flask-SQLAlchemy==2.3.2
flask-marshmallow==0.10.1
itsdangerous==1.1.0
Jinja2==2.10
gunicorn==19.9.0
marshmallow==2.19.5
marshmallow-sqlalchemy==0.17.0
MarkupSafe==1.1.1
psycopg2==2.7.7
psycopg2-binary==2.7.7
pycparser==2.19
python-dotenv==0.10.1
six==1.12.0
SQLAlchemy==1.3.1
virtualenv==16.1.0
virtualenv-clone==0.4.0
Werkzeug==0.15.1
WTForms==2.2.1
Pillow==6.1.0
